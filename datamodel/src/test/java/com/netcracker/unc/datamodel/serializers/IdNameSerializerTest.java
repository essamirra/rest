package com.netcracker.unc.datamodel.serializers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.unc.datamodel.entities.Student;
import org.junit.*;

public class IdNameSerializerTest {
    private static final ObjectMapper mapper = new ObjectMapper();
    private static Student student;

    @Before
    public void setUp() {
        System.out.println("Test is running");
    }

    @BeforeClass
    public static void setUpPrev() {
        student = new Student();
        student.setId(1);
        student.setName("Maxim");
        student.setFaculty("123");
        student.setUtilityInformation(123L);
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("Test is finished");
    }

    @Test
    public void usingIdNameSerializerShouldReturnOnlyIdNameFields() {
        JsonNode node = mapper.valueToTree(student);

        Assert.assertEquals(2, node.size());
        Assert.assertTrue(node.has("name"));
        Assert.assertTrue(node.has("id"));
    }
}
