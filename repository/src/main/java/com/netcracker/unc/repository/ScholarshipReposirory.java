package com.netcracker.unc.repository;

import com.netcracker.unc.datamodel.entities.Scholarship;
import com.netcracker.unc.datamodel.entities.Student;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ScholarshipReposirory {
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @PostConstruct
    public void init()
    {
        sessionFactory.openSession();
    }

    @Transactional
    public void insert(Scholarship scholarship)
    {
        sessionFactory.getCurrentSession().save(scholarship);
    }

    @PreDestroy
    public void destroy()
    {
        sessionFactory.close();
    }

    @Transactional
    public Scholarship getScholarship(Integer id) {
        return sessionFactory.getCurrentSession().get(Scholarship.class, id);
    }

    @Transactional
    public void update(Scholarship scholarship) {
        sessionFactory.getCurrentSession().update(scholarship);

    }
    @Transactional
    public void delete(Scholarship scholarship) {
        sessionFactory.getCurrentSession().delete(scholarship);
    }

}
