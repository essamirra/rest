package com.netcracker.unc.api.serialization;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.unc.datamodel.entities.BaseEntity;
import com.netcracker.unc.datamodel.entities.Student;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.List;

public class DatamodelSerializer {
    private static final ObjectMapper mapper = new ObjectMapper();

    public static ResponseEntity<String> serialize(BaseEntity entity) {
        JsonNode node = mapper.valueToTree(entity);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        return new ResponseEntity<>(node.toString(), headers, HttpStatus.OK);

    }

    public static ResponseEntity<String> serializeList(List<Student> entity) throws JsonProcessingException {

         HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);

        return new ResponseEntity<>(mapper.writeValueAsString(entity), headers, HttpStatus.OK);

    }
}
