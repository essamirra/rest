package com.netcracker.unc.repository;

import com.netcracker.unc.datamodel.entities.Scholarship;
import com.netcracker.unc.datamodel.entities.Student;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.Map;

public class StudentRepository {

    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @PostConstruct
    public void init()
    {
        sessionFactory.openSession();
    }

    @Transactional
    public void insert(Student student)
    {
        sessionFactory.getCurrentSession().save(student);
    }

    @PreDestroy
    public void destroy()
    {
        sessionFactory.close();
    }

    @Transactional
    public Student getStudent(Integer id) {
        return sessionFactory.getCurrentSession().get(Student.class, id);
    }

    @Transactional
    public void update(Student student) {
        sessionFactory.getCurrentSession().update(student);

    }
    @Transactional
    public void delete(Student student) {
        sessionFactory.getCurrentSession().delete(student);
    }
    @Transactional
    public List<Student> getFriends(Integer id) {
        return getStudent(id).getFriends();
    }
    @Transactional
    public List<Student> findByFacultyAndName(String faculty, String name) {
        Criteria c = sessionFactory.getCurrentSession().createCriteria(Student.class);

        if(faculty != null) {
            c.add(Restrictions.eq("faculty", faculty));
        }

        if(name != null) {
            c.add(Restrictions.eq("name", name));
        }

        return c.list();

    }

    @Transactional
    public Scholarship getStudentScholarhip(Integer id) {
        return getStudent(id).getsShip();
    }

}
