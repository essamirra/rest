package com.netcracker.unc.datamodel.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.netcracker.unc.datamodel.serializers.Aggregation;
import com.netcracker.unc.datamodel.serializers.IdNameSerializer;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)

@Entity
@Table
@JsonSerialize(using = IdNameSerializer.class)
public class Student extends BaseEntity {
    private String faculty;
    private List<Student> friends;
    private Long utilityInformation;

    private Scholarship sShip;

    @ManyToOne
    @PrimaryKeyJoinColumn
    @JsonSerialize(contentUsing = IdNameSerializer.class)
    public Scholarship getsShip() {
       return sShip;
    }
    public void setsShip(Scholarship sShip) {
       this.sShip = sShip;
   }

    @JsonProperty("department")
    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }
   @JsonSerialize(contentUsing = Aggregation.class)

    @OneToMany
   @PrimaryKeyJoinColumn
   public List<Student> getFriends() {
       return friends;
    }

   public void setFriends(List<Student> friends) {
       this.friends = friends;
   }

    @JsonIgnore
    public Long getUtilityInformation() {
        return utilityInformation;
    }

    public void setUtilityInformation(Long utilityInformation) {
        this.utilityInformation = utilityInformation;
    }
}
