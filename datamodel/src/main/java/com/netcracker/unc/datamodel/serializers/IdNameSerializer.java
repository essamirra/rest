package com.netcracker.unc.datamodel.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.netcracker.unc.datamodel.entities.BaseEntity;

import java.io.IOException;

public class IdNameSerializer extends JsonSerializer<BaseEntity> {
    @Override
    public void serialize(BaseEntity baseEntity, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();

        jsonGenerator.writeNumberField("id",baseEntity.getId());
        jsonGenerator.writeStringField("name",baseEntity.getName());

        jsonGenerator.writeEndObject();
    }
}
