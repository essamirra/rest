package com.netcracker.unc.datamodel.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.netcracker.unc.datamodel.entities.BaseEntity;

import java.io.IOException;

public class Aggregation extends JsonSerializer<BaseEntity> {
    @Override
    public void serialize(BaseEntity baseEntity, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeObject(baseEntity.getId());
    }
}
