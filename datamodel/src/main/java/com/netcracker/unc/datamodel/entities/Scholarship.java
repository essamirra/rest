package com.netcracker.unc.datamodel.entities;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.netcracker.unc.datamodel.serializers.IdNameSerializer;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table
public class Scholarship extends BaseEntity {


    private BigDecimal amount;
    private List<Student> students;
    private Semester semester;


    @JsonSerialize(contentUsing = ToStringSerializer.class)
    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @JsonSerialize(contentUsing = IdNameSerializer.class)


    @OneToMany(mappedBy = "sShip", fetch = FetchType.EAGER)
    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
