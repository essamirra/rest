package com.netcracker.unc.starter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.context.embedded.EmbeddedWebApplicationContext;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ImportResource;
import org.springframework.web.servlet.DispatcherServlet;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication(
        exclude = HibernateJpaAutoConfiguration.class
)
@ImportResource("classpath:applicationContext.xml")
public class WebApp extends SpringBootServletInitializer {

    @Autowired
    private EmbeddedWebApplicationContext embeddedWebApplicationContext;

    @Bean
    public Docket swaggerSettings() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .pathMapping("/api/v1");
    }

    @Bean
    public DispatcherServlet dispatcherServlet() {
        return new DispatcherServlet(embeddedWebApplicationContext);
    }

    @Bean
    public ServletRegistrationBean restApi() {
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(new DispatcherServlet(embeddedWebApplicationContext), "/api/v1/*");
        servletRegistrationBean.setName(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME);
        return servletRegistrationBean;
    }

    public static void main(String[] args) {
        SpringApplication.run(WebApp.class,args);
    }
}
