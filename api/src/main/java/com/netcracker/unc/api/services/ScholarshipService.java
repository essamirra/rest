package com.netcracker.unc.api.services;

import com.netcracker.unc.api.serialization.DatamodelSerializer;
import com.netcracker.unc.datamodel.entities.Scholarship;
import com.netcracker.unc.datamodel.entities.Student;
import com.netcracker.unc.repository.ScholarshipReposirory;
import com.netcracker.unc.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
@RequestMapping("/scholarship")
public class ScholarshipService {

    private ScholarshipReposirory repository;

    public ScholarshipReposirory getRepository() {
        return repository;
    }

    @Required
    public void setRepository(ScholarshipReposirory repository) {
        this.repository = repository;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> getScholarshipById(HttpServletRequest request, @PathVariable Integer id) {
        Scholarship scholarship = repository.getScholarship(id);
        return DatamodelSerializer.serialize(scholarship);
    }
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> insertScholarship(HttpServletRequest request,
                                                @RequestBody Scholarship scholarship)
    {
        repository.insert(scholarship);
        return new ResponseEntity<String>(HttpStatus.CREATED);
    }
    @RequestMapping(value="/update", method = RequestMethod.POST)
    public ResponseEntity<String> updateScholarship(HttpServletRequest request,
                                                    @RequestBody Scholarship scholarship)
    {
        repository.update(scholarship);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value="/delete",method = RequestMethod.POST)
    public ResponseEntity<String> deleteScholarship(HttpServletRequest request,
                                                    @RequestBody Scholarship scholarship)
    {
        repository.delete(scholarship);
        return new ResponseEntity<String>(HttpStatus.OK);
    }







}
