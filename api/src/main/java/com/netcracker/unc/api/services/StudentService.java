package com.netcracker.unc.api.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.netcracker.unc.api.serialization.DatamodelSerializer;
import com.netcracker.unc.datamodel.entities.Scholarship;
import com.netcracker.unc.datamodel.entities.Student;
import com.netcracker.unc.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@SuppressWarnings({"unused", "WeakerAccess"})
@RequestMapping("/student")
public class StudentService {
    private StudentRepository repository;

    public StudentRepository getRepository() {
        return repository;
    }

    @Required
    public void setRepository(StudentRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> getStudent(HttpServletRequest request, @PathVariable Integer id) {
        Student student = repository.getStudent(id);
        return DatamodelSerializer.serialize(student);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> insertStudent(HttpServletRequest request,
                                                @RequestBody Student student)
    {
        repository.insert(student);
        return new ResponseEntity<String>(HttpStatus.CREATED);
    }

    @RequestMapping(value="/update", method = RequestMethod.POST)
    public ResponseEntity<String> updateStudent(HttpServletRequest request,
                                                    @RequestBody Student student)
    {
        repository.update(student);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value="/delete",method = RequestMethod.POST)
    public ResponseEntity<String> deleteStudent(HttpServletRequest request,
                                                    @RequestBody Student student)
    {
        repository.delete(student);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value="/friends/{id}",method = RequestMethod.GET)
    public ResponseEntity<String> getStudentsByFacultyAndName(HttpServletRequest request,
                                                              @PathVariable Integer id)
    {
        try {
            return DatamodelSerializer.serializeList(repository.getFriends(id));
        } catch (JsonProcessingException e) {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }
    }


    @RequestMapping(value="faculty/{faculty}/name/{name}",method = RequestMethod.GET)
    public ResponseEntity<String> getStudentsByFacultyAndName(HttpServletRequest request,
                                                              @PathVariable String faculty,
                                                              @PathVariable String name)
    {
        try {
            return DatamodelSerializer.serializeList(repository.findByFacultyAndName(faculty, name));
        } catch (JsonProcessingException e) {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value="faculty/name/{name}",method = RequestMethod.GET)
    public ResponseEntity<String> getStudentsByName(HttpServletRequest request,

                                                              @PathVariable String name)
    {
        try {
            return DatamodelSerializer.serializeList(repository.findByFacultyAndName(null, name));
        } catch (JsonProcessingException e) {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }

    }
    @RequestMapping(value="faculty/{faculty}/name",method = RequestMethod.GET)
    public ResponseEntity<String> getStudentsByFaculty(HttpServletRequest request,
                                                              @PathVariable String faculty
                                                             )
    {
        try {
            return DatamodelSerializer.serializeList(repository.findByFacultyAndName(faculty, null));
        } catch (JsonProcessingException e) {
            return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "scholarship/{id}", method = RequestMethod.GET)
    public ResponseEntity<String> getStudentScholarship(HttpServletRequest request, @PathVariable Integer id) {
        Scholarship scholarship = repository.getStudentScholarhip(id);
        return DatamodelSerializer.serialize(scholarship);
    }


}
